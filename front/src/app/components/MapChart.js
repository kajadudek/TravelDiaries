"use client";

import React, { useContext, useEffect, useState } from "react";
import {
  ComposableMap,
  Geographies,
  Geography
} from "react-simple-maps";
import { Transition } from '@headlessui/react';
import PopUpWindow from "./dialog/popup-window";
import { PopupContext } from "../contexts/PopUp/popup-context";
import { useUserVisitedCountries } from "../contexts/Countries/visited-countries-context";

const geoUrl = "/features.json";

const MapChart = () => {
  const { visitedCountries } = useUserVisitedCountries();
  const { setCurrentStep, setSelectedCountry, selectedCountry } = useContext(PopupContext);

  const handleCountryClick = (geo) => {
    const countryCode = geo.id;
    const countryName = geo.properties.name;

    setSelectedCountry({ code: countryCode, name: countryName });
    setCurrentStep('selectDiary');
  };

  return (
    <div>
      <ComposableMap projectionConfig={{ rotate: [-10, 0, 0], scale: 160 }}>
        <Geographies geography={geoUrl}>
          {({ geographies }) =>
            geographies.map((geo) => {
              const isVisited = visitedCountries.some(country => country.countryCode === geo.id);
              const fillColor = isVisited ? "#ffa31a" : "#D3D3D3";
              return (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  onClick={() => handleCountryClick(geo)}
                  fill={fillColor}
                  style={{
                    default: { stroke: "#f2f2f2", strokeWidth: 0.5, outline: "none" },
                    hover: { fill: "#ff8000", outline: "#000000" },
                    pressed: { fill: "#b3b3b3", outline: "none" }
                  }}
                />
              );
            })
          }
        </Geographies>
      </ComposableMap>
      <Transition show={!!selectedCountry && !!selectedCountry.code} enter="transition-opacity duration-300" enterFrom="opacity-0" enterTo="opacity-100">
        <PopUpWindow/>
      </Transition>
    </div>
  );
};

export default MapChart;
