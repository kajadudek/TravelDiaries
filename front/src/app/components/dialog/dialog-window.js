import CloseDialogButton from "../buttons/close-dialog-button";

const DialogWindow = ({ children, title, description, onCloseFunction }) => {
    return (
        <div className="fixed inset-0 bg-black bg-opacity-50 flex justify-center items-center w-full">
            <div className="relative bg-white p-5 rounded-lg shadow-lg overflow-hidden max-h-[95vh] lg:min-h-[80vh] lg:min-w-[84vh]">
                <CloseDialogButton onClickFunction={onCloseFunction} />

                <div className="space-y-12">
                    <div className="border-b border-gray-900/10 pb-8">
                        <h2 className="text-xl font-semibold leading-7 text-gray-900">{title}</h2>
                        <p className="mt-1 text-sm leading-6 text-gray-600">{description}</p>
                    </div>
                </div>

                {children}
            </div>
        </div>
    );
}

export default DialogWindow;