import React, { useState, useContext, useEffect } from 'react';
import DialogWindow from './dialog-window';
import DiaryList from '../diary/diary-list';
import AddEntryForm from '../diaryEntry/add-entry';
import DiaryEntryList from '../diaryEntry/diary-entry-list';
import { PopupContext } from '@/app/contexts/PopUp/popup-context';

const PopUpWindow = () => {
    const { currentStep, setCurrentStep, setSelectedCountry, setSelectedDiary, selectedCountry, selectedDiary } = useContext(PopupContext);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');

    useEffect(() => {
        switch (currentStep) {
            case 'selectDiary':
                setTitle(`Travel to ${selectedCountry.name}`);
                setDescription("Select diary to add your new travel");
                break;
            case 'showDiaryEntries':
                setTitle(`Travel to ${selectedCountry.name}`);
                setDescription((<>Add new travel to <strong>{selectedDiary.name}</strong> diary</>));
                break;
            case 'addEntry':
                setTitle(`Travel to ${selectedCountry.name}`);
                setDescription((<>Add new travel to <strong>{selectedDiary.name}</strong> diary</>));
                break;
            default:
                setTitle('');
                setDescription('');
        }
    }, [currentStep, selectedCountry, selectedDiary]);

    const getContent = () => {
        switch (currentStep) {
            case 'selectDiary':
                return <DiaryList onSelectDiary={setSelectedDiary} />;
            case 'showDiaryEntries':
                return <DiaryEntryList selectedDiaryId={selectedDiary.id} />;
            case 'addEntry':
                return <AddEntryForm diaryId={selectedDiary.id} closeForm={() => setCurrentStep('showDiaryEntries')} />;
            default:
                return null;
        }
    };

    const closeWindow = () => {
        setSelectedCountry(null);
        setSelectedDiary({ id: null, name: null });
        setCurrentStep('selectCountry');
    };

    return (
        <>
            {selectedCountry && (
                <DialogWindow title={title} description={description} onCloseFunction={closeWindow}>
                    {getContent()}
                </DialogWindow>
            )}
        </>
    );
};

export default PopUpWindow;
