const DiaryPreview = ({diaryName, lastModifiedDate, selectDiaryFunction}) => {

    const formatDate = (dateString) => {
        const date = new Date(dateString);
        const formattedDate = date.toLocaleDateString();
        const formattedTime = date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
        return `${formattedDate} ${formattedTime}`;
    };

    return (
        <tr>
            <td className="px-6 py-4 align-middle whitespace-nowrap hidden md:table-cell">
                <img className="h-10 w-10 rounded-full" src="https://img.freepik.com/free-vector/suitcase-hat-seascape-scene_603843-3531.jpg" alt="" />
            </td>
            <td className="px-6 py-4 align-middle">
                <div className="text-sm text-gray-900">{diaryName}</div>
                <div className="text-sm text-gray-500">description</div>
            </td>
            <td className="px-6 py-4 align-middle text-sm text-gray-500 hidden md:table-cell">
                {formatDate(lastModifiedDate)}
            </td>
            <td className="px-6 py-4 align-middle text-sm font-medium">
                <a onClick={selectDiaryFunction} className="text-indigo-600 hover:text-indigo-900">Select</a>
                <a href="#" className="ml-2 text-red-600 hover:text-red-900">Delete</a>
            </td>
        </tr>
    )
}

export default DiaryPreview;