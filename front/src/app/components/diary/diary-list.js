import { useContext, useState, useEffect } from "react";
import DiaryPreview from "./diary-preview";
import { getUsersDiaries } from "@/app/services/diaryAPIService";
import { PopupContext } from "@/app/contexts/PopUp/popup-context";


const DiaryList = () => {
    const { setCurrentStep, setSelectedDiary, selectedCountry} = useContext(PopupContext);
    const [diaries, setDiaries] = useState([]);
    const userId = 1; // Hardcoded user ID

    useEffect(() => {
        if (selectedCountry) {
            getUsersDiaries(userId)
                .then(data => {
                    setDiaries(data);
                })
                .catch(error => {
                    console.error("Fetching error:", error);
                });
        }
    }, [selectedCountry]);

    const handleDiarySelect = (diary) => {
        setSelectedDiary({ id: diary.diaryId, name: diary.name });
        setCurrentStep('showDiaryEntries');
    }

    if (!selectedCountry) return null;

    return (
        <>
            <table className="min-w-full divide-y divide-gray-200 overflow-x-auto">
                <thead className="bg-gray-50">
                    <tr>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden md:flex">
                            Diary
                        </th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Name
                        </th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden md:flex">
                            Last modified
                        </th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"></th>
                    </tr>
                </thead>

                <tbody className="bg-white divide-y divide-gray-200">
                    {diaries.map((diary, index) => (
                        <DiaryPreview 
                            key={index} 
                            diaryName={diary.name} 
                            lastModifiedDate={diary.updatedAt} 
                            selectDiaryFunction={() => handleDiarySelect(diary)}>
                        </DiaryPreview>
                    ))}
                </tbody>
            </table>
        </>
    )
}

export default DiaryList;