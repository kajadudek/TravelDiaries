
import { useContext, useState } from "react";
import CloseDialogButton from "../buttons/close-dialog-button";
import { PopupContext } from "@/app/contexts/PopUp/popup-context";
import { useUserVisitedCountries } from "@/app/contexts/Countries/visited-countries-context";
import { addNewDiaryEntry } from "@/app/services/diaryAPIService";


const AddEntryForm = ({diaryId, closeForm}) => {
    const { currentStep, setCurrentStep, selectedCountry } = useContext(PopupContext);
    const { addVisitedCountry } = useUserVisitedCountries();
    const [title, setTitle] = useState('');
    const [dateFrom, setDateFrom] = useState('');
    const [dateTo, setDateTo] = useState('');
    const [description, setDescription] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();

        const diaryEntry = {
            diary: { diaryId },
            country: { 
                countryCode: selectedCountry.code, 
                name: selectedCountry.name 
            },
            title,
            dateFrom,
            dateTo,
            description
        };

        console.log(diaryEntry);

        try {
            const newDiaryEntry = await addNewDiaryEntry(diaryEntry);
            addVisitedCountry(newDiaryEntry.country.countryCode);
            closeForm();
        } catch (error) {
            console.error("Error submitting new diary entry:", error);
        }
    };

    return (
        <form onSubmit={handleSubmit} className="relative bg-white p-5 mt-5 rounded-lg shadow-md overflow-auto max-h-[65vh]">
                <CloseDialogButton onClickFunction={closeForm}></CloseDialogButton>

                <div className="space-y-5">
                    <div className="border-b border-gray-900/10 pb-12">
                        <h2 className="text-base font-semibold leading-7 text-gray-900">{selectedCountry.name}</h2>
                        <p className="mt-1 text-sm leading-6 text-gray-600">
                            Fill in the details of your new diary entry.
                        </p>
                    </div>

                    <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                        <div className="sm:col-span-6">
                            <label htmlFor="title" className="block text-sm font-medium leading-6 text-gray-900">
                                Travel title
                            </label>
                            <div className="mt-2">
                                <input
                                    type="text"
                                    name="title"
                                    id="title"
                                    onChange={(e) => setTitle(e.target.value)}
                                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-0 focus:ring-inset focus:ring-gray-600 sm:text-sm sm:leading-6"
                                />
                            </div>
                        </div>

                        <div className="sm:col-span-3">
                            <label htmlFor="dateFrom" className="block text-sm font-medium leading-6 text-gray-900">
                                Start Date
                            </label>
                            <div className="mt-2">
                                <input
                                    type="date"
                                    name="dateFrom"
                                    id="dateFrom"
                                    onChange={(e) => setDateFrom(e.target.value)}
                                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-0 focus:ring-inset focus:ring-gray-600 sm:text-sm sm:leading-6"
                                />
                            </div>
                        </div>

                        <div className="sm:col-span-3">
                            <label htmlFor="dateTo" className="block text-sm font-medium leading-6 text-gray-900">
                                End Date
                            </label>
                            <div className="mt-2">
                                <input
                                    type="date"
                                    name="dateTo"
                                    id="dateTo"
                                    onChange={(e) => setDateTo(e.target.value)}
                                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-0 focus:ring-inset focus:ring-gray-600 sm:text-sm sm:leading-6"
                                />
                            </div>
                        </div>

                        <div className="sm:col-span-6">
                            <label htmlFor="description" className="block text-sm font-medium leading-6 text-gray-900">
                                Description (max 5000 characters)
                            </label>
                            <div className="mt-2">
                                <textarea
                                    name="description"
                                    id="description"
                                    rows="5"
                                    maxLength="5000"
                                    onChange={(e) => setDescription(e.target.value)}
                                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-0 focus:ring-inset focus:ring-gray-600 sm:text-sm sm:leading-6"
                                ></textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" onSubmit={handleSubmit} className="text-white bg-prim-orange hover:bg-darker-orange focus:bg-orange-600 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2">Submit</button>
                </div>

        </form>
    )

}

export default AddEntryForm;