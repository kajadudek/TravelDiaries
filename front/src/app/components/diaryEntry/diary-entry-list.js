import { useContext, useState, useEffect } from "react";
import { deleteDiaryEntry, getDiaryEntries } from "@/app/services/diaryAPIService";
import DiaryEntryPreview from "./diary-entry-preview";
import { PopupContext } from "@/app/contexts/PopUp/popup-context";
import { useUserVisitedCountries } from "@/app/contexts/Countries/visited-countries-context";


const DiaryEntryList = ({ selectedDiaryId }) => {
    const { setCurrentStep } = useContext(PopupContext);
    const { removeVisitedCountry } = useUserVisitedCountries();
    const [diaryEntries, setDiaryEntries] = useState([]);

    useEffect(() => {
        if (selectedDiaryId) {
            getDiaryEntries(selectedDiaryId)
                .then(data => {
                    setDiaryEntries(data);
                })
                .catch(error => {
                    console.error("Fetching error:", error);
                });
        }
    }, [selectedDiaryId]);

    const handleAddEntryClick = () => {
        setCurrentStep('addEntry')
    }

    const deleteEntry = (entry) => {
        deleteDiaryEntry(entry)
            .then(() => {
                setDiaryEntries((prevDiaryEntries) =>
                    prevDiaryEntries.filter((dEntry) => dEntry !== entry)
                );
                removeVisitedCountry(entry.country.countryCode);
            })
            .catch((error) => {
                console.error("Error deleting diary entry:", error);
            });
    }

    if (!selectedDiaryId) return null;

    return (
        <>
            <button 
                className="my-3 text-white bg-prim-orange hover:bg-darker-orange focus:bg-orange-600 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2"
                onClick={handleAddEntryClick}>
                Add new diary entry
            </button>

            <table className="my-5 min-w-full divide-y divide-gray-200 overflow-x-auto">
                <thead className="bg-gray-50">
                    <tr>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                            Name
                        </th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden md:table-cell">
                            Date
                        </th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider hidden md:flex">
                            Country
                        </th>
                        <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"></th>
                    </tr>
                </thead>

                <tbody className="bg-white divide-y divide-gray-200">
                    {diaryEntries.map((entry, index) => (
                        <DiaryEntryPreview
                            key={index} 
                            entry={entry}
                            onDelete={() => deleteEntry(entry)}
                        ></DiaryEntryPreview>
                    ))}
                </tbody>
            </table>
        </>
    )
}

export default DiaryEntryList;