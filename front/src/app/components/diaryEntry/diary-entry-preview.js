const DiaryEntryPreview = ({entry, onDelete}) => {

    const formatDate = (dateString) => {
        const date = new Date(dateString);
        const formattedDate = date.toLocaleDateString();
        return `${formattedDate}`;
    };

    const truncateText = (text) => {
        if (text.length > 15) {
            return text.substring(0, 15) + "...";
        } else {
            return text;
        }
    };

    return (
        <tr>
            <td className="px-6 py-4 align-middle whitespace-nowrap">
                <div className="text-sm text-gray-900">{entry.title}</div>
            </td>
            <td className="px-6 py-4 align-middle hidden md:table-cell">
                <div className="text-sm text-gray-900">{formatDate(entry.dateFrom)} - {formatDate(entry.dateTo)}</div>
            </td>
            <td className="px-6 py-4 align-middle text-sm text-gray-500 hidden md:table-cell">
                <div className="text-sm text-gray-500">{truncateText(entry.country.name)}</div>
            </td>
            <td className="px-6 py-4 align-middle text-sm font-medium">
                <a onClick={onDelete} className="ml-2 text-red-600 hover:text-red-900">Delete</a>
            </td>
        </tr>
    )
}

export default DiaryEntryPreview;