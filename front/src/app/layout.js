import { Inter } from 'next/font/google'
import './globals.css'
import { PopupProvider } from './contexts/PopUp/popup-context'
import { UserVisitedCountriesProvider } from './contexts/Countries/visited-countries-context'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'Travel Diaries',
  description: 'TravelDiaries app',
}

export default function RootLayout({ children }) {
  const userId = 1; //Hardcoded for now

  return (
    <html lang="en">
      <body className={inter.className}>
        <UserVisitedCountriesProvider  userId={userId}>
          <PopupProvider>
            {children}          
          </PopupProvider>
        </UserVisitedCountriesProvider>
        
      </body>
    </html>
  )
}
