"use client";

import MapChart from './components/MapChart.js';
import { useEffect } from 'react';
import fetchAndPostCountries from './utils/fetch-countries.js';

export default function Home() {

  // USE TO POST COUNTRIES TO DB
  // useEffect(() => {
  //   fetchAndPostCountries();
  // }, [])

  return (
    <main className="flex flex-col items-center justify-between">
      
      <div className="max-w-screen-xl w-full max-h-min px-5">
        <MapChart/>
      </div>

      <div className="grid text-center lg:max-w-5xl lg:w-full lg:mb-0 lg:grid-cols-4 lg:text-left">
        <a
          href="https://nextjs.org/docs?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Your diaries{' '}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
        </a>

      </div>
    </main>
  )
}
