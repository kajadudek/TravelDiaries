import countriesData from '../../resources/features.json';

const postCountriesToDatabase = async (countries) => {
    try {
        const response = await fetch(`http://localhost:8222/api/v1/countries`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(countries)
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const responseData = await response.json();
        console.log("Countries posted to database:", responseData);
    } catch (error) {
        console.error("Error posting countries to database:", error);
    }
};

const fetchAndPostCountries = () => {
    const countries = countriesData.objects.world.geometries
        .map(feature => ({
            countryCode: feature.id, 
            name: feature.properties.name
        }));

    postCountriesToDatabase(countries);
};

export default fetchAndPostCountries;
