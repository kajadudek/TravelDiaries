import axios from 'axios';

const API_BASE_URL='http://localhost:8222/api/v1'



export const getUsersDiaries = (userId) => {
    return axios.get(`${API_BASE_URL}/diaries/users/${userId}`, {
        headers: {
            'Content-Type': 'application/json',
        },
    })
    .then(response => response.data)
    .catch(error => { throw error; });
};

export const getDiaryEntries = (selectedDiaryId) => {
    return axios.get(`${API_BASE_URL}/diaries/${selectedDiaryId}/entries`, {
        headers: {
            'Content-Type': 'application/json',
        },
    })
    .then(response => response.data)
    .catch(error => { throw error; });
};

export const getUsersVisitedCountries = (userId) => {
    return axios.get(`${API_BASE_URL}/users/${userId}/countries`, {
        headers: {
            'Content-Type': 'application/json',
        },
    })
    .then(response => response.data)
    .catch(error => { throw error; });
}

export const addNewDiaryEntry = (diaryEntry) => {
    return axios.post(`${API_BASE_URL}/diaries/${diaryEntry.diary.diaryId}/entries`, diaryEntry, {
        headers: {
            'Content-Type': 'application/json',
        },
    })
    .then(response => response.data)
    .catch(error => { throw error; });
};

export const deleteDiaryEntry = (diaryEntry) => {
    console.log(diaryEntry);
    return axios.delete(`${API_BASE_URL}/diaries/${diaryEntry.diary.diaryId}/entries/${diaryEntry.entryId}`)
    .then(response => response.data)
    .catch(error => { throw error; });
};

