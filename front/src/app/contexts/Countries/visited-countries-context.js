"use client";

import { getUsersVisitedCountries } from '@/app/services/diaryAPIService';
import React, { createContext, useContext, useState, useEffect } from 'react';

const UserVisitedCountriesContext = createContext();

export const useUserVisitedCountries = () => useContext(UserVisitedCountriesContext);

export const UserVisitedCountriesProvider = ({ children, userId }) => {
    const [visitedCountries, setVisitedCountries] = useState([]);

    useEffect(() => {
        getUsersVisitedCountries(userId)
            .then(data => {
                console.log(data);
                setVisitedCountries(data.map(country => ({ 
                    countryCode: country.countryCode, 
                    visitCount: country.visitCount 
                })));
            })
            .catch(error => console.error("Failed to fetch countries", error));
    }, [userId]);

    const addVisitedCountry = (countryCode) => {
        setVisitedCountries(prev => {
            const countryIndex = prev.findIndex(country => country.countryCode === countryCode);
            if (countryIndex !== -1) {
                const newCountries = [...prev];
                newCountries[countryIndex] = { ...newCountries[countryIndex], visitCount: newCountries[countryIndex].visitCount + 1 };
                console.log(newCountries);
                return newCountries;
            } else {
                return [...prev, { countryCode, visitCount: 1 }];
            }
        });
    };

    const removeVisitedCountry = (countryCode) => {
        setVisitedCountries(prev => {
            const countryIndex = prev.findIndex(country => country.countryCode === countryCode);
            if (countryIndex !== -1 && prev[countryIndex].visitCount > 1) {
                const newCountries = [...prev];
                newCountries[countryIndex] = { ...newCountries[countryIndex], visitCount: newCountries[countryIndex].visitCount - 1 };
                return newCountries;
            } else if (countryIndex !== -1 && prev[countryIndex].visitCount === 1) {
                return prev.filter(country => country.countryCode !== countryCode);
            }
            return prev;
        });
    };
    
    return (
        <UserVisitedCountriesContext.Provider value={{ visitedCountries, addVisitedCountry, removeVisitedCountry }}>
            {children}
        </UserVisitedCountriesContext.Provider>
    );
};
