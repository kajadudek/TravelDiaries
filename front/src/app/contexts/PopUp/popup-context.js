"use client";

import { createContext, useState } from "react";
export const PopupContext = createContext();

export const PopupProvider = ({ children }) => {
    const [selectedCountry, setSelectedCountry] = useState(null);
    const [selectedDiary, setSelectedDiary] = useState(null);
    const [currentStep, setCurrentStep] = useState('selectCountry');

    const contextValue = {
        selectedCountry,
        setSelectedCountry,
        selectedDiary,
        setSelectedDiary,
        currentStep,
        setCurrentStep,
    };

    return (
        <PopupContext.Provider value={contextValue}>
            {children}
        </PopupContext.Provider>
    );
};

