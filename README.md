# Travel Diaries

## Introduction

Travel Diaries is a web application I'm currently developing, designed for travelers to document and share their travel experiences. This project aims to allow users to select countries they've traveled to, post stories, photos, and display their journeys on an interactive map. It's built with Spring Boot for the backend and React for the frontend, ensuring a robust and scalable application.

## Technology Stack

### Backend
- **Java 17**
- **Spring Boot 3.2**
- **Spring Cloud**
- **SQL H2 Database**

### Frontend
- **React**
- **JavaScript**
- **Tailwind CSS**

## Backend Microservices Overview

- **Diary Service**: Manages diaries and its entries, including creating, updating, and retrieving user diaries and travel posts.
- **User Service**: Handles user profile management.
- **Security Service**: Provides authentication and authorization functionalities using JWT tokens to secure the application.
- **Config Server**: Centralizes external configuration management across all microservices, facilitating easy updates and consistency.
- **Discovery Service**: Acts as a service registry to enable microservices to discover and communicate with each other dynamically.
- **Gateway Service**: An entry point for the backend, routing requests to the appropriate microservices. Created to provide functionalities like load balancing.

## Current Focus

I am currently working on implementing security and authentication features using JWT tokens on the 'security' branch. This is a critical aspect of the project, ensuring that user data is protected and that access control measures are in place.

## Features

- **Country Selection and Posting**: Users can select the countries they have traveled to and post their experiences.
- **Interactive Maps**: The application displays the user's visited countries on an interactive map, providing a visual representation of their journeys.
- **Security**: Currently implementing JWT for authentication and authorization to secure the application and user data.

## Future features
- **Contenarization**
- **Logging in**
- **Data validation**
- **Uploading photos**
- **Data visualisation**


## Installation

Follow these steps to set up the project locally:

```bash
# Clone the repository
git clone https://gitlab.com/kajadudek/TravelDiaries.git

# Navigate to the project directory
cd TravelDiaries

# Make sure you have Maven and Java 17 installed
# Follow frontend and backend setup described below
```

### Frontend Setup

To set up the frontend of the Travel Diaries application, follow these steps:

1. Navigate to the `front` directory from the root of the project:
   ```bash
   cd front
2. Install the necessary npm packages:
    ```bash
    npm install

3. Start the frontend application:
    ```bash
    npm start
The application will run on http://localhost:3000. Open it in web browser to view the frontend side.

### Backend Setup
The backend of Travel Diaries consists of multiple microservices, including **diary, user, security_service, config_server, discovery**, and **gateway**. 
Each microservice must be started individually for the application to function properly.

Follow these steps for each microservice:

1. Navigate to the `back` directory from the root of the project:
    ```bash
    cd back

2. Inside the `back` directory, you will find separate directories for each microservice. Navigate into each microservice that you need to start by:

    ```bash
    cd [microservice_name]

3. Replace **[microservice_name]** with the name of the microservice directory (discovery, gateway etc.)

4. Start each microservice using Maven:

    ```bash
    mvn spring-boot:run

The gateway microservice, will run on http://localhost:8222. 
**This is the entry point for the backend services.**

Please ensure all required microservices are running to enable full functionality of the Travel Diaries application.
