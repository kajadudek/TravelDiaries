package com.webapp.travel_diaries.entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DiaryEntryService {

    @Autowired
    DiaryEntryRepository diaryEntryRepository;

    public List<DiaryEntry> findEntriesByDiaryId(int diaryId) {
        return diaryEntryRepository.findByDiary_DiaryId(diaryId);
    }

    public DiaryEntry saveEntry(DiaryEntry diaryEntry) {
        return diaryEntryRepository.save(diaryEntry);
    }

    public List<DiaryEntry> findDiaryEntriesByCountry(int diaryId, String countryCode) {
        return diaryEntryRepository.findDiaryEntriesByDiary_DiaryId_AndCountry_CountryCode(diaryId, countryCode);
    }

    public void deleteDiaryEntryById(int diaryEntryId) {
        diaryEntryRepository.deleteById(diaryEntryId);
    }

    public Optional<DiaryEntry> findDiaryEntryById(int diaryEntryId) {
        return diaryEntryRepository.findById(diaryEntryId);
    }
}
