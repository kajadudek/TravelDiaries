package com.webapp.travel_diaries.entry;

import com.webapp.travel_diaries.diary.Diary;
import com.webapp.travel_diaries.map.Country;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class DiaryEntry {
    @Id
    @GeneratedValue
    private int entryId;

    @ManyToOne
    @JoinColumn(name = "diary_id", referencedColumnName = "diaryId")
    private Diary diary;

    @ManyToOne
    @JoinColumn(name = "country_code", referencedColumnName = "countryCode")
    private Country country;

    private String title;

    @DateTimeFormat
    private LocalDate dateFrom;
    @DateTimeFormat
    private LocalDate dateTo;
    @Column(length = 5000)
    private String description;
    @CreatedDate
    private LocalDateTime createdAt;

    @Override
    public String toString() {
        return "DiaryEntry{" +
                "entryId=" + entryId +
                ", diary=" + diary +
                ", country=" + country +
                ", title='" + title + '\'' +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", description='" + description + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
