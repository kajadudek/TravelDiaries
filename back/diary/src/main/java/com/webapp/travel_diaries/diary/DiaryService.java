package com.webapp.travel_diaries.diary;

import com.webapp.travel_diaries.entry.DiaryEntryRepository;
import com.webapp.travel_diaries.map.VisitedCountriesResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DiaryService {

    private final DiaryRepository diaryRepository;
    private final DiaryEntryRepository diaryEntryRepository;

    public Optional<Diary> getDiaryById(int id) {
        return diaryRepository.findById(id);
    }

    public List<Diary> getAllUserDiaries(int userId) {
        return diaryRepository.findByUserId(userId);
    }

    public List<VisitedCountriesResponse> getUsersVisitedCountries(int userId) {
        List<Diary> diaries = diaryRepository.findByUserId(userId);
        return diaryEntryRepository.findDistinctCountriesByDiaryIds(diaries);
    }

    public Diary saveDiary(Diary diary) {
        return diaryRepository.save(diary);
    }
}
