package com.webapp.travel_diaries.diary;

import com.webapp.travel_diaries.entry.DiaryEntry;
import com.webapp.travel_diaries.entry.DiaryEntryRepository;
import com.webapp.travel_diaries.map.Country;
import com.webapp.travel_diaries.map.CountryRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Configuration
public class DiaryConfiguration {

    @Bean
    CommandLineRunner commandLineRunner(DiaryRepository diaryRepository, CountryRepository countryRepository, DiaryEntryRepository diaryEntryRepository) {
        return args -> {
            if (diaryRepository.count() == 0) {


//                User user = new User(1, "name", "mail");
//                userRepository.save(user);
                Diary usersDiary = new Diary(1, "2024", 1, LocalDateTime.now());
                Diary usersDiary2 = new Diary(2, "2024 - winter", 1, LocalDateTime.now());

                Country country = new Country("USA", "United States");
                countryRepository.save(country);
                diaryRepository.save(usersDiary);
                diaryRepository.save(usersDiary2);

                DiaryEntry usersDiaryEntry = new DiaryEntry(
                        1,
                        usersDiary,
                        country,
                        "Super trip",
                        LocalDate.ofYearDay(2024, 1),
                        LocalDate.ofYearDay(2024, 12),
                        "",
                        LocalDateTime.now());

                diaryEntryRepository.save(usersDiaryEntry);
            }
        };
    }
}
