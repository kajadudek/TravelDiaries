package com.webapp.travel_diaries.map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CountryService {

    @Autowired
    CountryRepository countryRepository;

    public Optional<Country> getCountryById(String countryId) {
        return countryRepository.findById(countryId);
    }

    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    public List<Country> saveCountries(List<Country> countries) {
        return this.countryRepository.saveAll(countries);
    }
}
