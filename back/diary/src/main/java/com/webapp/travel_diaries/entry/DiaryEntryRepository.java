package com.webapp.travel_diaries.entry;

import com.webapp.travel_diaries.diary.Diary;
import com.webapp.travel_diaries.map.VisitedCountriesResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DiaryEntryRepository extends JpaRepository<DiaryEntry, Integer> {
    List<DiaryEntry> findByDiary_DiaryId(int diaryId);

    List<DiaryEntry> findDiaryEntriesByDiary_DiaryId_AndCountry_CountryCode(int diaryId, String countryCode);

    @Query("SELECT new com.webapp.travel_diaries.map.VisitedCountriesResponse(c.countryCode, COUNT(de)) " +
            "FROM DiaryEntry de " +
            "JOIN de.country c " +
            "WHERE de.diary IN :diaries " +
            "GROUP BY c.countryCode")
    List<VisitedCountriesResponse> findDistinctCountriesByDiaryIds(List<Diary> diaries);
}
