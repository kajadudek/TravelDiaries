package com.webapp.travel_diaries.diary;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/diaries")
@RequiredArgsConstructor
public class DiaryController {

    private final DiaryService diaryService;

    @GetMapping("/{id}")
    public ResponseEntity<Diary> getDiaryById(@PathVariable int id) {
        return diaryService.getDiaryById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<List<Diary>> getAllDiaries(@PathVariable int userId) {
        List<Diary> usersDiaries = diaryService.getAllUserDiaries(userId);
        return ResponseEntity.ok(usersDiaries);
    }

    @PostMapping("/users/{userId}")
    public ResponseEntity<Diary> createDiary(@PathVariable int userId, @RequestBody Diary diary) {
        try {
            Diary newDiary = diaryService.saveDiary(diary);
            return ResponseEntity.status(HttpStatus.CREATED).body(newDiary);
        } catch (DataAccessException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
