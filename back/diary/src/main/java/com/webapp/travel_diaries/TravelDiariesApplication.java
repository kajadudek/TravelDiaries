package com.webapp.travel_diaries;

import com.webapp.travel_diaries.diary.DiaryConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
@Import({DiaryConfiguration.class})
public class TravelDiariesApplication {

    public static void main(String[] args) {
        SpringApplication.run(TravelDiariesApplication.class, args);
    }

}
