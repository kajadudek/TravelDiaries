package com.webapp.travel_diaries.entry;

import com.webapp.travel_diaries.map.Country;
import com.webapp.travel_diaries.map.CountryService;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/diaries/{diaryId}/entries")
public class DiaryEntryController {

    DiaryEntryService diaryEntryService;
    CountryService countryService;

    @GetMapping()
    public ResponseEntity<List<DiaryEntry>> getAllDiaryEntries(@PathVariable int diaryId) {
        List<DiaryEntry> diaryEntries = diaryEntryService.findEntriesByDiaryId(diaryId);
        return ResponseEntity.ok(diaryEntries);
    }

    @GetMapping("/byCountry/{countryCode}")
    public ResponseEntity<List<DiaryEntry>> getAllDiaryEntriesFromCountry(@PathVariable int diaryId, @PathVariable String countryCode) {
        List<DiaryEntry> diaryEntries = diaryEntryService.findDiaryEntriesByCountry(diaryId, countryCode);
        return ResponseEntity.ok(diaryEntries);
    }

    @PostMapping
    public ResponseEntity<DiaryEntry> createDiaryEntry(@PathVariable int diaryId, @RequestBody DiaryEntry diaryEntry) {
        try {
            Optional<Country> country = countryService.getCountryById(diaryEntry.getCountry().getCountryCode());
            if (country.isPresent()) {
                DiaryEntry newDiaryEntry = diaryEntryService.saveEntry(diaryEntry);
                return ResponseEntity.ok(newDiaryEntry);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (DataAccessException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{diaryEntryId}")
    public ResponseEntity<Void> deleteDiaryEntry(@PathVariable int diaryId, @PathVariable int diaryEntryId) {
        Optional<DiaryEntry> diaryEntry = diaryEntryService.findDiaryEntryById(diaryEntryId);

        if (diaryEntry.isPresent() && diaryEntry.get().getDiary().getDiaryId() == diaryId) {
            diaryEntryService.deleteDiaryEntryById(diaryEntryId);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
