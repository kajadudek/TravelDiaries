package com.webapp.travel_diaries.map;

import com.webapp.travel_diaries.diary.DiaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/countries")
public class CountryController {

    private final CountryService countryService;
    private final DiaryService diaryService;

    @GetMapping
    public ResponseEntity<List<Country>> getCountries() {
        List<Country> countryList = countryService.getAllCountries();
        return ResponseEntity.ok(countryList);
    }

    @GetMapping("/{countryId}")
    public ResponseEntity<Country> getCountryById(@PathVariable String countryId) {
        return countryService.getCountryById(countryId)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<List<VisitedCountriesResponse>> getAllVisitedCountries(@PathVariable int userId) {
        List<VisitedCountriesResponse> visitedCountries = diaryService.getUsersVisitedCountries(userId);
        return ResponseEntity.ok(visitedCountries);
    }

    @PostMapping
    public ResponseEntity<List<Country>> saveAllCountries(@RequestBody List<Country> countries) {
        try {
            countryService.saveCountries(countries);
            return ResponseEntity.ok(countries);
        } catch (DataAccessException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
