package com.webapp.travel_diaries.map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VisitedCountriesResponse {
    private String countryCode;
    private long visitCount;

    public VisitedCountriesResponse(String countryCode, long visitCount) {
        this.countryCode = countryCode;
        this.visitCount = visitCount;
    }
}
