package com.travel_diaries.user.user;

import com.travel_diaries.user.client.CountryClient;
import com.travel_diaries.user.responses.Country;
import com.travel_diaries.user.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final CountryClient countryClient;

    public Optional<User> findById(int id) {
        return userRepository.findById(id);
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public List<Country> getAllVisitedCountries(int userId) {
        return countryClient.findUsersVisitedCountries(userId);
    }
}
