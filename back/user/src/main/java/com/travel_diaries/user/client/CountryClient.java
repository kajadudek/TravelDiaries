package com.travel_diaries.user.client;

import com.travel_diaries.user.responses.Country;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "country-service", url="${application.config.country-url}")
public interface CountryClient {
    @GetMapping("/users/{userId}")
    List<Country> findUsersVisitedCountries(@PathVariable int userId);
}
